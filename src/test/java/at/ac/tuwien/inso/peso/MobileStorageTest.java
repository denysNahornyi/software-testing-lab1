package at.ac.tuwien.inso.peso;

import org.apache.commons.lang.StringUtils;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Tests the implementation of MobileStorage
 */
public class MobileStorageTest {


    private MobileStorage mobileStorage;
    private final int STORAGE_SIZE = 10;

    @BeforeMethod(groups = {"testingMobileMessageClass"})
    public void doBeforeEachMethod() {
       mobileStorage = new MobileStorage(STORAGE_SIZE);
    }

    private final String MESSAGE_TEXT = "Message Text";
    private final String ANOTHER_MESSAGE_TEXT = "Another Message Text";


    @Test(groups ={"testingMobileMessageClass"})
    public void test_creatingMobileMessageWithoutPredecessorAndCheckingEqualsOfMessage() {
        MobileMessage mobileMessage = new MobileMessage(MESSAGE_TEXT,null);
        Assert.assertEquals(mobileMessage.getText(), MESSAGE_TEXT);
    }

    @Test(groups ={"testingMobileMessageClass"})
    public void test_creatingMobileMessageWithoutPredecessorAndCheckingNotEqualsOfMessage() {
        MobileMessage mobileMessage = new MobileMessage(MESSAGE_TEXT,null);
        Assert.assertNotEquals(mobileMessage.getText(), ANOTHER_MESSAGE_TEXT);
    }

    @Test(groups ={"testingMobileMessageClass"})
    public void test_creatingMobileMessageWithOnePredecessorAndCheckingEqualsOfBothMessages() {
        MobileMessage firstMobileMessage = new MobileMessage(MESSAGE_TEXT,null);
        MobileMessage secondMobileMessage = new MobileMessage(ANOTHER_MESSAGE_TEXT, firstMobileMessage);
        Assert.assertEquals(secondMobileMessage.getText(), ANOTHER_MESSAGE_TEXT);
        Assert.assertEquals(secondMobileMessage.getPredecessor().getText(), MESSAGE_TEXT);
    }

    @Test(groups = {"testingMobileStorageClass"}, expectedExceptions = {IllegalArgumentException.class})
    public void test_gettingExceptionWhileCreatingMobileStorageWithIncorrectSize() {
        mobileStorage = new MobileStorage(0);
    }

    @Test(groups = {"testingMobileStorageClass"})
    public void test_possibilityToCreateMobileStorageWithSizeEqualOne() {
        mobileStorage = new MobileStorage(1);
    }


    @Test(groups = {"testingMobileStorageClass"})
    public void test_gettingOccupied() {
        mobileStorage.saveMessage(MESSAGE_TEXT);
        Assert.assertEquals(1,mobileStorage.getOccupied());
    }

    @Test(groups = {"testingMobileStorageClass"})
    public void test_gettingOccupiedWithDoubleMessage() {
        mobileStorage.saveMessage(MESSAGE_TEXT);
        mobileStorage.saveMessage(StringUtils.repeat(MESSAGE_TEXT, 20));
        Assert.assertNotEquals(2,mobileStorage.getOccupied());
        Assert.assertEquals(3,mobileStorage.getOccupied());
    }


    @Test(groups = {"testingMobileStorageClass"})
    public void test_gettingOccupiedAfterDeletingMessage() {
        mobileStorage.saveMessage(MESSAGE_TEXT);
        mobileStorage.saveMessage(ANOTHER_MESSAGE_TEXT);
        Assert.assertEquals(2,mobileStorage.getOccupied());
        mobileStorage.deleteMessage();
        Assert.assertNotEquals(2,mobileStorage.getOccupied());
        Assert.assertEquals(1,mobileStorage.getOccupied());
    }


    @Test(groups = {"testingMobileStorageClass"})
    public void test_gettingEmptyListMessages() {
        Assert.assertEquals(mobileStorage.listMessages(),"");
    }

    @Test(groups = {"testingMobileStorageClass"})
    public void test_gettingListMessagesWhileThereIsOnlyOneMessage() {
        mobileStorage.saveMessage(MESSAGE_TEXT);
        Assert.assertEquals(mobileStorage.listMessages(),MESSAGE_TEXT);
    }

    @Test(groups = {"testingMobileStorageClass"})
    public void test_gettingListMessages() {
        mobileStorage.saveMessage(MESSAGE_TEXT);
        mobileStorage.saveMessage(ANOTHER_MESSAGE_TEXT);
        Assert.assertEquals(mobileStorage.listMessages(),MESSAGE_TEXT+"\n"+ANOTHER_MESSAGE_TEXT);
    }

    @Test(groups = {"testingMobileStorageClass"})
    public void test_gettingListMessagesWhileTheStorageIsFull() {
        for (int i = 0; i < STORAGE_SIZE; i++)mobileStorage.saveMessage(MESSAGE_TEXT);

        Assert.assertEquals(mobileStorage.listMessages(),StringUtils.repeat(MESSAGE_TEXT + "\n",9)+ MESSAGE_TEXT);
    }


    @Test(groups = {"testingMobileStorageClass"})
    public void test_gettingListMessagesWithDoubleMessage() {
        mobileStorage.saveMessage(MESSAGE_TEXT);
        mobileStorage.saveMessage(StringUtils.repeat(MESSAGE_TEXT,20));
        Assert.assertEquals(mobileStorage.listMessages(),MESSAGE_TEXT + "\n" + StringUtils.repeat(MESSAGE_TEXT,20));
    }

    @Test(groups = {"testingMobileStorageClass"})
    public void test_savingMessage() {
        mobileStorage.saveMessage(MESSAGE_TEXT);
        Assert.assertEquals(MESSAGE_TEXT, mobileStorage.listMessages());
    }

    @Test(groups = {"testingMobileStorageClass"})
    public void test_savingMessagesOnCorrectPositions() {
        mobileStorage.saveMessage(MESSAGE_TEXT);
        mobileStorage.saveMessage(ANOTHER_MESSAGE_TEXT);
        String finalMessagesString = MESSAGE_TEXT + "\n" + ANOTHER_MESSAGE_TEXT;
        Assert.assertEquals(finalMessagesString, mobileStorage.listMessages());
    }

    @Test(groups = {"testingMobileStorageClass"})
    public void test_savingLongMessage() {
        String longMessage = StringUtils.repeat(MESSAGE_TEXT, 20);
        mobileStorage.saveMessage(longMessage);
        Assert.assertEquals(longMessage, mobileStorage.listMessages());
    }

    @Test(groups = {"testingMobileStorageClass"},expectedExceptions = {IllegalArgumentException.class})
    public void test_notSavingEmptyMessage() {
        mobileStorage.saveMessage("");
        Assert.assertEquals("", mobileStorage.listMessages());
    }

    @Test(groups = {"testingMobileStorageClass"})
    public void test_savingLongMessagesOnCorrectPositions() {
        String longMessage = StringUtils.repeat(MESSAGE_TEXT, 20);
        mobileStorage.saveMessage(longMessage);
        String anotherLongMessage = StringUtils.repeat(ANOTHER_MESSAGE_TEXT, 20);
        mobileStorage.saveMessage(anotherLongMessage);
        String finalMessageString = longMessage + "\n" + anotherLongMessage;
        Assert.assertEquals(finalMessageString, mobileStorage.listMessages());
    }


    @Test(groups = {"testingMobileStorageClass"}, expectedExceptions = {RuntimeException.class})
    public void test_gettingExceptionAfterSavingMoreThatStorageSizeMessages() {
            for (int i = 0; i < STORAGE_SIZE + 1; i++) mobileStorage.saveMessage(MESSAGE_TEXT);
    }

    @Test(groups = {"testingMobileStorageClass"}, expectedExceptions = {RuntimeException.class})
    public void test_gettingExceptionAfterSavingMessageThatRequiresStorageMoreThanIs() {
        for (int i = 0; i < STORAGE_SIZE -1; i++) mobileStorage.saveMessage(MESSAGE_TEXT);
        mobileStorage.saveMessage(StringUtils.repeat(MESSAGE_TEXT,20));
    }

    @Test(groups = {"testingMobileStorageClass"})
    public void test_noExceptionAfterSavingLastMessageWithMaximalSize() {
        for (int i = 0; i < STORAGE_SIZE -1; i++) mobileStorage.saveMessage(MESSAGE_TEXT);
        mobileStorage.saveMessage(StringUtils.repeat("a",160));
        Assert.assertEquals(STORAGE_SIZE,mobileStorage.getOccupied());
    }

    @Test(groups = {"testingMobileStorageClass"})
    public void test_savingFirstMessagesWhileStorageIsFree(){
        try {
            for (int i = 0; i < STORAGE_SIZE + 5; i++) mobileStorage.saveMessage(MESSAGE_TEXT);
        } catch (RuntimeException ignored) {}
        Assert.assertEquals(StringUtils.repeat( MESSAGE_TEXT + "\n", 9) + MESSAGE_TEXT, mobileStorage.listMessages());
    }


    @Test(groups = {"testingMobileStorageClass"}, expectedExceptions = {RuntimeException.class})
    public void test_impossibilityOfSavingDoubleMessageWhileThereIsOnlyOneFreeSpace() {
        for (int i = 0; i < STORAGE_SIZE - 1; i++) mobileStorage.saveMessage(MESSAGE_TEXT);
        Assert.assertEquals(StringUtils.repeat(MESSAGE_TEXT + "\n",8) + MESSAGE_TEXT, mobileStorage.listMessages());
        mobileStorage.saveMessage(StringUtils.repeat(MESSAGE_TEXT,20));
    }

    @Test(groups = {"testingMobileStorageClass"}, expectedExceptions = {RuntimeException.class})
    public void test_impossibilityToSaveSuperLongMessage() {
        mobileStorage.saveMessage(StringUtils.repeat(MESSAGE_TEXT,14*STORAGE_SIZE));
    }

    @Test(groups = {"testingMobileStorageClass"})
    public void test_deletingTheOnlyOneMessage() {
        mobileStorage.saveMessage(MESSAGE_TEXT);
//        mobileStorage.saveMessage(MESSAGE_TEXT);
        mobileStorage.deleteMessage();
        Assert.assertEquals(mobileStorage.listMessages(), "");
    }

    @Test(groups = {"testingMobileStorageClass"})
    public void test_deletingMessage() {
        mobileStorage.saveMessage(MESSAGE_TEXT);
        mobileStorage.saveMessage(MESSAGE_TEXT);
        mobileStorage.deleteMessage();
        Assert.assertEquals(mobileStorage.listMessages(), MESSAGE_TEXT);
    }

    @Test(groups = {"testingMobileStorageClass"})
    public void test_deletingOfCorrectLastMessage() {
        mobileStorage.saveMessage(MESSAGE_TEXT);
        mobileStorage.saveMessage(ANOTHER_MESSAGE_TEXT);
        mobileStorage.deleteMessage();
        Assert.assertEquals(mobileStorage.listMessages(), ANOTHER_MESSAGE_TEXT);
    }

    @Test(groups = {"testingMobileStorageClass"})
    public void test_deletingPartOfLongMessage() {
        mobileStorage.saveMessage(StringUtils.repeat(MESSAGE_TEXT,20));
        mobileStorage.deleteMessage();
        Assert.assertNotEquals(mobileStorage.listMessages(), "");
    }

    @Test(groups = {"testingMobileStorageClass"})
    public void test_possibilityToAddNewMessageAfterRiddingFirstMessage() {

        for (int i = 0; i < STORAGE_SIZE; i++) mobileStorage.saveMessage(MESSAGE_TEXT);
        mobileStorage.deleteMessage();
        mobileStorage.saveMessage(ANOTHER_MESSAGE_TEXT);
        Assert.assertNotEquals(mobileStorage.listMessages(), StringUtils.repeat(MESSAGE_TEXT,9)+ANOTHER_MESSAGE_TEXT);
    }

    @Test(groups = {"testingMobileStorageClass"}, expectedExceptions = {RuntimeException.class})
    public void test_gettingExceptionWhileDeletingMessageInEmptyStorage() {
        mobileStorage.deleteMessage();
    }

    @Test(groups = {"testingMobileStorageClass"})
    public void test_searchingOccurrences() {
        mobileStorage.saveMessage(MESSAGE_TEXT);
        Assert.assertEquals(1 , mobileStorage.searchOccurrences("age"));
    }

    @Test(groups = {"testingMobileStorageClass"}, expectedExceptions = {RuntimeException.class})
    public void test_gettingExceptionWhileSearchCriteriaIsNull() {
        mobileStorage.saveMessage(MESSAGE_TEXT);
        Assert.assertEquals(1,mobileStorage.searchOccurrences(null));
    }

    @Test(groups = {"testingMobileStorageClass"})
    public void test_searchingSeveralOccurrences() {
        mobileStorage.saveMessage(MESSAGE_TEXT);
        mobileStorage.saveMessage(ANOTHER_MESSAGE_TEXT);
        Assert.assertEquals(2,mobileStorage.searchOccurrences("age"));
    }

    @Test(groups = {"testingMobileStorageClass"})
    public void test_searchingOccurrencesWithNotExistentCriteria() {
        mobileStorage.saveMessage(MESSAGE_TEXT);
        int result = mobileStorage.searchOccurrences("abc");
        Assert.assertEquals(0,result);
    }

    @Test(groups = {"testingMobileStorageClass"})
    public void test_searchingOccurrencesWithExistentCriteriaButWithCapitalLetters() {
        mobileStorage.saveMessage(MESSAGE_TEXT);
        Assert.assertEquals(0,mobileStorage.searchOccurrences("AGE"));
        Assert.assertEquals(0,mobileStorage.searchOccurrences("Age"));
        Assert.assertEquals(0,mobileStorage.searchOccurrences("agE"));
        Assert.assertEquals(0,mobileStorage.searchOccurrences("aGe"));
    }

    @Test(groups = {"testingMobileStorageClass"})
    public void test_searchingSeveralOccurrencesInOneMessage() {
        mobileStorage.saveMessage(MESSAGE_TEXT + " " + MESSAGE_TEXT);
        Assert.assertEquals(1,mobileStorage.searchOccurrences("age")); //In PDF file stays that method returns amount of messages that contains criteria (in this case right) or it returns amount of message parts (in this case false). 1 || 0 -> 1
    }

    @Test(groups = {"testingMobileStorageClass"})
    public void test_searchingOccurrencesWhileCriteriaIsEscapeCharacter() {
        mobileStorage.saveMessage(MESSAGE_TEXT+'\u039A');
        Assert.assertEquals(1,mobileStorage.searchOccurrences(String.valueOf('\u039A')));
    }

    @Test(groups = {"testingMobileStorageClass"})
    public void test_searchingOccurrencesWhileCriteriaIsEscapeCharacterSecondTest() {
        mobileStorage.saveMessage(MESSAGE_TEXT + "\\");
        Assert.assertEquals(1,mobileStorage.searchOccurrences("\\"));
    }

    @Test(groups = {"testingMobileStorageClass"})
    public void test_searchingMessageByCriteria() {
        mobileStorage.saveMessage(MESSAGE_TEXT);
        Assert.assertEquals(MESSAGE_TEXT ,mobileStorage.search("age"));
    }

    @Test(groups = {"testingMobileStorageClass"})
    public void test_searchingMessagesByCriteria() {
        mobileStorage.saveMessage(MESSAGE_TEXT);
        mobileStorage.saveMessage(ANOTHER_MESSAGE_TEXT);
        Assert.assertEquals(MESSAGE_TEXT + "\n" + ANOTHER_MESSAGE_TEXT ,mobileStorage.search("age"));
    }

    @Test(groups = {"testingMobileStorageClass"})
    public void test_ImpossibilityToFindMessageByIncorrectCriteria() {
        mobileStorage.saveMessage(MESSAGE_TEXT);
        Assert.assertNotEquals(MESSAGE_TEXT ,mobileStorage.search("abc"));
    }

    @Test(groups = {"testingMobileStorageClass"})
    public void test_ImpossibilityToFindMessageByCorrectCriteriaButInUpperCase() {
        mobileStorage.saveMessage(MESSAGE_TEXT);
        Assert.assertNotEquals(MESSAGE_TEXT ,mobileStorage.search("Age"));
        Assert.assertNotEquals(MESSAGE_TEXT ,mobileStorage.search("AGe"));
        Assert.assertNotEquals(MESSAGE_TEXT ,mobileStorage.search("AGE"));
        Assert.assertNotEquals(MESSAGE_TEXT ,mobileStorage.search("AgE"));
    }

    @Test(groups = {"testingMobileStorageClass"})
    public void test_EmptySearchingResultWhileTestCriteriaIsNull() {
        mobileStorage.saveMessage(MESSAGE_TEXT);
        Assert.assertNotEquals(mobileStorage.search(null), MESSAGE_TEXT);
        Assert.assertEquals("" , mobileStorage.search(null));
        mobileStorage.saveMessage(ANOTHER_MESSAGE_TEXT);
        Assert.assertNotEquals(mobileStorage.search("\n"), MESSAGE_TEXT+"\n"+ANOTHER_MESSAGE_TEXT);
    }

    @Test(groups = {"testingMobileStorageClass"})
    public void test_ContainingAllMessagesInResultWhileTestCriteriaIsEmpty() {
        mobileStorage.saveMessage(MESSAGE_TEXT);
        mobileStorage.saveMessage(ANOTHER_MESSAGE_TEXT);
        Assert.assertEquals(mobileStorage.search(""), MESSAGE_TEXT + "\n"+ANOTHER_MESSAGE_TEXT);
    }

    @Test(groups = {"testingMobileStorageClass"})
    public void test_FindingWholeMessageWhileMessageIsDouble() {
        mobileStorage.saveMessage(StringUtils.repeat(MESSAGE_TEXT,20));
        Assert.assertEquals(StringUtils.repeat(MESSAGE_TEXT,20) ,mobileStorage.search("age"));
    }

    @Test(groups = {"testingMobileStorageClass"})
    public void test_ContainingTheResultOnlyOneMultipartMessageWhileMessageContainsCriteriaSeveralTimes() {
        mobileStorage.saveMessage(StringUtils.repeat(MESSAGE_TEXT,20));
        Assert.assertEquals(StringUtils.repeat(MESSAGE_TEXT,20) ,mobileStorage.search("age"));
        Assert.assertNotEquals(StringUtils.repeat(MESSAGE_TEXT,20) ,StringUtils.repeat(mobileStorage.search("age"),20));
    }

    @Test(groups = {"testingMobileStorageClass"})
    public void test_ContainingTheResultOnlyOneMessageWhileMessageContainsCriteriaSeveralTimes() {
        mobileStorage.saveMessage(StringUtils.repeat(MESSAGE_TEXT,2));
        Assert.assertEquals(StringUtils.repeat(MESSAGE_TEXT,2) ,mobileStorage.search("age"));
        Assert.assertNotEquals(StringUtils.repeat(MESSAGE_TEXT,20) ,StringUtils.repeat(mobileStorage.search("age"),2));
    }

}
